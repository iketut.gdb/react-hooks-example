import React, {useState} from "react";
import 'antd/dist/antd.css'
import ShowModal from "./Component/ShowModal/ShowModal";
import {Col, Input, Row} from "antd";
import {useInput} from "./Hooks/UseInput/UseInput";
import {CoolContextProvider} from "./Context/CoolContext";
import ViewText from "./ViewText";
import SetViewText from "./SetViewText";

const App = () => {
    const {value:limit , onChange: handleOnLimitChange} = useInput(5);
    const {value: name, onChange: handleOnNameChange} = useInput('');
    const [showModal, setShowModal] = useState(false);

    const handleModalOpen = () => {
        setShowModal(true);
    };

    const handleOnCancel = () => {
        setShowModal(false);
    };

    return (
        <Row>
            <Col span={12}>
                <Row>
                    <Col span={6}>
                        <Input
                            value={limit}
                            onChange={handleOnLimitChange}
                            placeholder='Change Limit'
                        />
                        <span></span>
                        <Input
                            value={name}
                            onChange={handleOnNameChange}
                            placeholder='Change Name'
                        />
                    </Col>
                    <button onClick={handleModalOpen}>Open Modal</button>
                    <ShowModal
                        showModal={showModal}
                        handleOnCancel={handleOnCancel}
                        limit={limit}
                    />
                    <CoolContextProvider>
                        <ViewText/>
                        <SetViewText/>
                    </CoolContextProvider>
                </Row>
            </Col>
        </Row>
    );
};

export default App;
