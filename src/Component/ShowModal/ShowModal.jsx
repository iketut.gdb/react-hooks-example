import React, {useEffect} from 'react';
import {Modal, Spin} from "antd";
import PokemonList from "../../PokemonList";
import {useFetch} from "../../Hooks/UseFetch/UseFetch";

const ShowModal = ({showModal, handleOnCancel, limit}) => {
    const { fetchedData: { data }, loading, callReFetch } = useFetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}`)

    useEffect(() => {
        callReFetch();
    }, [limit])

    return (
        <div>
            <Modal
                visible={showModal}
                onCancel={handleOnCancel}
            >
                <h1>Pokemon List</h1>
                <Spin spinning={loading}>
                    {data && <PokemonList pokemonData={data.results}/>}
                </Spin>
            </Modal>
        </div>
    );
};

export default ShowModal;
