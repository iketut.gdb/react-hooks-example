import React, { createContext, useContext, useState } from 'react';

const CoolContext = createContext([]);
const useCoolContext = () => useContext(CoolContext);

const CoolContextProvider = (props) => {
    const { children } = props;
    const [coolText, setCoolText] = useState('');

    return (
        <CoolContext.Provider
            value={{
                coolText,
                setCoolText
            }}
        >
            {children}
        </CoolContext.Provider>
    );
};

export {
    useCoolContext,
    CoolContextProvider
};
