import React from 'react';
import { shallow } from 'enzyme';
import EnzymeToJson from 'enzyme-to-json';
import {CoolContextProvider} from "./CoolContext";

describe('CoolContext', () => {
    describe('#render', () => {
        it('should render component CoolContext correctly', () => {
            const renderedComponent = shallow(
                <CoolContextProvider>
                    <div>Component</div>
                </CoolContextProvider>
            );

            const actualComponent = EnzymeToJson(renderedComponent);

            expect(actualComponent).toMatchSnapshot();
        });
    });
});
