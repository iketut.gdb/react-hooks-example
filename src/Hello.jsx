import React, {memo} from "react";
import { useCountRenders } from "./Hooks/UseCountRenders.jsx";

export const Hello = memo(({ increment }) => {
    useCountRenders();

    return <button onClick={increment}>hello</button>;
});
