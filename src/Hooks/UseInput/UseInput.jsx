import { useState } from "react";

const useInput = (initialState) => {
    const [value, setValue] = useState(initialState);

    const handleInput = (e) => {
        setValue(e.target.value);
    }
    return {
        value,
        onChange: handleInput
    };
}

export {
    useInput
}
