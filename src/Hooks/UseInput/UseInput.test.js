import {renderHook} from '@testing-library/react-hooks';
import {useInput} from "./UseInput";

describe('UseInput', () => {
    it('should return value baskara and onChange function when initial value is baskara', async () => {
        const {result: actualResult} = renderHook(() => useInput('baskara'));

        expect(actualResult.current)
            .toEqual(
                expect
                    .objectContaining(
                        {value: 'baskara'}
                        )
            );
    });
});
