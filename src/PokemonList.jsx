import React from 'react';

const PokemonList = ({ pokemonData }) => {
    return (
        <ul style={{'list-style-type': 'none'}}>
            { pokemonData.map((pokemon, index) => <li>{index + 1}.{pokemon.name}</li>) }
        </ul>
    );
};

export default PokemonList;
