import React, {useState} from 'react';
import {Input, Row} from "antd";
import {useCoolContext} from "./Context/CoolContext";
import {useInput} from "./Hooks/UseInput/UseInput";

const SetViewText = () => {
   const { setCoolText } = useCoolContext();
   const [ value, onChange ] = useState('')

    const handleChangeViewText = (e) => {
        onChange(e.target.value)
        setCoolText(e.target.value)
    }
    return (
        <div>
            <Input
                value={value}
                onChange={handleChangeViewText}
                placeholder='Change Limit'
            />
        </div>
    );
};

export default SetViewText;
