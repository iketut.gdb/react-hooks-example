import React, {useEffect} from 'react';
import {useCoolContext} from "./Context/CoolContext";

const ViewText = () => {
   const { coolText} = useCoolContext()
    return (
        <div>
            {coolText && coolText }
        </div>
    );
};

export default ViewText;
